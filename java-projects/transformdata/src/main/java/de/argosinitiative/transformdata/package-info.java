/*
 * 
 *
 * Copyright (c) 2018, Capgemini UK PLC.
 *
 * This project includes software developed by Capgemini UK PLC.
 *
 * https://www.uk.capgemini.com/
 *
 * Licensed under the Capgemini Enterprise iPaaS Subscription Agreement Version 1.0.
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
/**
 * This is the root package.
 *
 */
package de.argosinitiative.transformdata;
