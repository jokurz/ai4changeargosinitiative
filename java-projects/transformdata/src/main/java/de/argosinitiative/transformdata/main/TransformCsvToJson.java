package de.argosinitiative.transformdata.main;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import de.argosinitiative.transformdata.model.DeepARTimeseries;
import de.argosinitiative.transformdata.model.RetailData;
import org.apache.commons.collections.list.TreeList;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import de.argosinitiative.transformdata.model.DeepARTimeseries.PredictionRole;

public class TransformCsvToJson {

    private static final String SAMPLE_CSV_FILE_PATH = "./OnlineRetail_full_cleaned_with_cat.csv";


    //csv options
    private static final int numSkipLines = 1;
    private static final char separator = ';';

    //deepAR forecasting
    private static final long binSizeInMs = 7*24*60*60*1000;
    //number of bins to use for testing, will cut the last testBatchSize bins and writes it to a separate file
    private static final int testBatchSize = 4;
    //context size: length of timeseries used for training, if testBatchSize + contextSize > full timeseries it will not be written to any file
    private static final int contextSize = 4;
    public static final int CSV_NUM_LINES_TO_READ = Integer.MAX_VALUE;

    public static Writer writerTraingData;
    public static Writer writerTestData;


    public static void main(String[] args) throws IOException {



        HashMap<String, TreeList> retailDataByStockcode = readInCsvGroupedByStockcode();
        createTimeseriesJsonList(retailDataByStockcode, binSizeInMs, PredictionRole.TARGET, PredictionRole.NONE);
        createTimeseriesJsonList(retailDataByStockcode, binSizeInMs, PredictionRole.NONE, PredictionRole.TARGET);

    }

    private static void createTimeseriesJsonList(HashMap<String, TreeList> retailDataByStockcode, long binSizeInMs,PredictionRole roleAmount, PredictionRole rolePrice){

        int counter = 0;
        writerTraingData = null;
        writerTestData = null;
        try {
            writerTraingData = new FileWriter("Amount_"+roleAmount.name()+"_Price_"+rolePrice+"_cat_training.json", true);
            writerTestData = new FileWriter("Amount_"+roleAmount.name()+"_Price_"+rolePrice+"_cat_test.json" , true);
        } catch (IOException e) {
            e.printStackTrace();
        }

        for(TreeList tl : retailDataByStockcode.values()){
          ArrayList<RetailData>[] binnedData = binRetailDataByTime(tl, binSizeInMs);
          DeepARTimeseries timeseries =  createTimeSeries(binnedData, roleAmount, rolePrice);
          if(timeseries.getTimeSeriesLength() < testBatchSize + contextSize){
              continue;
          }
          DeepARTimeseries[] splitTimeseries = timeseries.splitTimeSeries(timeseries.getTimeSeriesLength() - testBatchSize, binSizeInMs);
          writeTimeseriesToFile(splitTimeseries[0], writerTraingData);
          if(splitTimeseries[1] != null)
            writeTimeseriesToFile(splitTimeseries[1], writerTestData);

          counter++;
          if(counter%500 == 0) {
              System.out.println("Written " + counter + " timeseries to File");
              try {
                  writerTraingData.flush();
              } catch (IOException e) {
                  e.printStackTrace();
              }
          }

        }

        try {
            writerTraingData.close();
            writerTestData.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static int getIndex(long startTime, long eventTime, long binSizeInMs){
        int result = (int)((eventTime-startTime)/binSizeInMs);
        return result;
    }

    private static ArrayList<RetailData>[]  binRetailDataByTime (TreeList list, long binSizeInMs){

        long startTime = ((RetailData)list.get(0)).getInvoiceDate().getTime();
        long endTime = ((RetailData)list.get(list.size()-1)).getInvoiceDate().getTime();

        int numBins = (int)(Math.ceil((endTime - startTime)/(float)binSizeInMs))+1;

        ArrayList<RetailData>[] binnedRetailData = new ArrayList[numBins];

        Iterator<RetailData> iterator = list.iterator();

        while(iterator.hasNext()) {
            RetailData retailData = iterator.next();
            long eventTime = retailData.getInvoiceDate().getTime();
            int binIndex = getIndex(startTime, eventTime, binSizeInMs);
            ArrayList<RetailData> bin = binnedRetailData[binIndex];
            if(bin == null){
                bin = new ArrayList<>();
                binnedRetailData[binIndex] = bin;
            }
            bin.add(retailData);
        }
        return binnedRetailData;

    }

    private static DeepARTimeseries createTimeSeries(ArrayList<RetailData>[] binnedRetailData, PredictionRole roleAmount, PredictionRole rolePrice ){

        DeepARTimeseries timeseries = new DeepARTimeseries();

        ArrayList<Double> quantaties = new ArrayList<>();
        ArrayList<Double> unitPrices = new ArrayList<>();

        for(ArrayList<RetailData> bin : binnedRetailData){
            double meanPrize = Double.NaN;
            double summedAmount = Double.NaN;

            if(bin != null && !bin.isEmpty()){
                 meanPrize = 0;
                 summedAmount = 0;

                Iterator<RetailData> iterator = bin.iterator();
                while (iterator.hasNext()) {
                    RetailData retailData = iterator.next();
                    meanPrize += retailData.getUnitPrice();
                    summedAmount += retailData.getQuantity();
                }
                meanPrize = (int)((100*meanPrize)/ bin.size());
            }

            quantaties.add(summedAmount);
            unitPrices.add(meanPrize);
        }

        timeseries.addData(timeseries, roleAmount, quantaties);
        timeseries.addData(timeseries, rolePrice, unitPrices);

        List<Integer> categories = new ArrayList<>();
        categories.add((int)binnedRetailData[0].get(0).getCategoryByDescription());
        timeseries.setCat(categories);

        Date startDate = binnedRetailData[0].get(0).getInvoiceDate();
        timeseries.setStart(startDate);


        return timeseries;

    }



    private static void writeTimeseriesToFile(DeepARTimeseries timeseries, Writer writer) {

        ObjectMapper objectMapper = new ObjectMapper();
        try {
            String json = objectMapper.writeValueAsString(timeseries);
            writer.write(json + "\n");

        } catch (JsonProcessingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private static HashMap<String, TreeList> readInCsvGroupedByStockcode() {
        HashMap<String, TreeList> retailDataByStockcode = new HashMap<>();

        int counter = 0;

        try (
                Reader reader = Files.newBufferedReader(Paths.get(SAMPLE_CSV_FILE_PATH));
        ) {
            CsvToBean<RetailData> csvToBean = new CsvToBeanBuilder<RetailData>(reader)
                    .withType(RetailData.class)
                    .withIgnoreLeadingWhiteSpace(true)
                    .withSeparator(separator)
                    .withSkipLines(numSkipLines)
                    .build();

            Iterator<RetailData> csvUserIterator = csvToBean.iterator();

            while (csvUserIterator.hasNext()) {
                RetailData retailData = csvUserIterator.next();

                if(retailDataByStockcode.containsKey(retailData.getStockCode())){
                    TreeList sortedEvents = retailDataByStockcode.get(retailData.getStockCode());
                    sortedEvents.add(retailData);
                }
                else{
                    TreeList sortedEvents = new TreeList();
                    sortedEvents.add(retailData);
                    retailDataByStockcode.put(retailData.getStockCode(), sortedEvents);
                }
                counter++;
                if(counter %1000 == 0)
                    System.out.println("Imported: "+counter+ " entries from csv file.");
                if(counter > CSV_NUM_LINES_TO_READ){
                    break;
                }
            }

        }
        catch (Exception e){
            System.err.println("While processing line "+counter+", Exception occured "+ e);
        }
        return retailDataByStockcode;
    }

}

