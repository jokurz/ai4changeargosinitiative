/*
 * 
 *
 * Copyright (c) 2018, Capgemini UK PLC.
 *
 * This project includes software developed by Capgemini UK PLC.
 *
 * https://www.uk.capgemini.com/
 *
 * Licensed under the Capgemini Enterprise iPaaS Subscription Agreement Version 1.0.
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package de.argosinitiative.transformdata.model;


import com.opencsv.bean.CsvBindByPosition;
import com.opencsv.bean.CsvDate;

import java.util.Date;

public class RetailData implements Comparable{
  @CsvBindByPosition(position = 2)
  private String invoiceNo;
  @CsvBindByPosition(position = 3)
  private String stockCode;
  @CsvBindByPosition(position = 4)
  private String description;
  @CsvBindByPosition(position = 5)
  private int quantity;
  @CsvBindByPosition(position = 6)
  @CsvDate("dd.MM.yyyy HH:mm")
  private Date invoiceDate;
  @CsvBindByPosition(position = 7)
  private float unitPrice;
  @CsvBindByPosition(position = 8)
  private String customerId;
  @CsvBindByPosition(position = 9)
  private String country;
  @CsvBindByPosition(position = 10)
  private double categoryByDescription;



  public String getInvoiceNo() {
    return invoiceNo;
  }

  public void setInvoiceNo(String invoiceNo) {
    this.invoiceNo = invoiceNo;
  }

  public String getStockCode() {
    return stockCode;
  }

  public void setStockCode(String stockCode) {
    this.stockCode = stockCode;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public int getQuantity() {
    return quantity;
  }

  public void setQuantity(int quantity) {
    this.quantity = quantity;
  }

  public Date getInvoiceDate() {
    return invoiceDate;
  }

  public void setInvoiceDate(Date invoiceDate) {
    this.invoiceDate = invoiceDate;
  }

  public float getUnitPrice() {
    return unitPrice;
  }

  public void setUnitPrice(float unitPrice) {
    this.unitPrice = unitPrice;
  }

  public String getCustomerId() {
    return customerId;
  }

  public void setCustomerId(String customerId) {
    this.customerId = customerId;
  }

  public String getCountry() {
    return country;
  }

  public void setCountry(String country) {
    this.country = country;
  }

  @Override
  public int compareTo(Object o) {
    if (o instanceof RetailData){
      RetailData otherRetailData = (RetailData) o;
      return (this.invoiceDate.before(otherRetailData.invoiceDate))? -1 : 1;
    }
    return 0;
  }

  public double getCategoryByDescription() {
    return categoryByDescription;
  }

  public void setCategoryByDescription(double categoryByDescription) {
    this.categoryByDescription = categoryByDescription;
  }
}
