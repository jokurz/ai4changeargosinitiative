package de.argosinitiative.transformdata.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.*;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class DeepARTimeseries {


    public enum PredictionRole {
        TARGET, FEATURE, NONE, CATEGORY
    }

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    Date start;
    @JsonProperty
    List<Double> target;
    @JsonProperty
    List<Integer> cat;
    @JsonProperty
    List<List<Double>> dynamic_feat;


    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public List<Double> getTarget() {
        return target;
    }

    public void setTarget(List<Double> target) {
        this.target = target;
    }

    public List<Integer> getCat() {
        return cat;
    }

    public void setCat(List<Integer> cat) {
        this.cat = cat;
    }

    public List<List<Double>> getDynamic_feat() {
        return dynamic_feat;
    }

    public void setDynamic_feat(List<List<Double>> dynamic_feat) {
        this.dynamic_feat = dynamic_feat;
    }

    public void addData(DeepARTimeseries timeseries, PredictionRole role, List<Double> listToAdd){
        if(PredictionRole.TARGET == role){
            timeseries.setTarget(listToAdd);
        }
        else if (PredictionRole.FEATURE == role){
            timeseries.getDynamic_feat().add(listToAdd);
        }
    }

    public DeepARTimeseries[] splitTimeSeries(int binIndex, long binSizeInMs){
        if(binIndex < 0 || binIndex >= target.size()-1){
            DeepARTimeseries[] splitTimeSeries = new DeepARTimeseries[2];
            splitTimeSeries[0] = this;
            return splitTimeSeries;
        }
        DeepARTimeseries partOne = new DeepARTimeseries();
        DeepARTimeseries partTwo = new DeepARTimeseries();

        partOne.setStart((Date) start.clone());
        Date partTwoStartDate = new Date(start.getTime() + binSizeInMs * binIndex);
        partTwo.setStart(partTwoStartDate);

        partOne.setTarget(target.subList(0,binIndex));
        partTwo.setTarget(target.subList(binIndex, target.size()));

        if(dynamic_feat != null) {
            partOne.setDynamic_feat(new ArrayList<List<Double>>());
            partTwo.setDynamic_feat(new ArrayList<List<Double>>());
            Iterator<List<Double>> iterator = dynamic_feat.iterator();
            while (iterator.hasNext()) {
                List<Double> dynamicFeat = iterator.next();
                partOne.getDynamic_feat().add(dynamicFeat.subList(0, binIndex));
                partTwo.getDynamic_feat().add(dynamicFeat.subList(binIndex , target.size()));
            }
        }

        partOne.setCat(this.cat);
        partTwo.setCat(this.cat);

        DeepARTimeseries[] splitTimeSeries = new DeepARTimeseries[2];
        splitTimeSeries[0] = partOne;
        splitTimeSeries[1] = partTwo;

        return splitTimeSeries;
    }

    @JsonIgnore
    public int getTimeSeriesLength(){
        return target.size();
    }
}
