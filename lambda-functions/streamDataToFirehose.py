import json
import boto3
import os
import io

FIREHOSE_DELIVERY_STREAM = os.environ['FIREHOSE_DELIVERY_STREAM']

def lambda_handler(event, context):
   
    print("Received event: " + json.dumps(event, indent=2))
    
    data = json.loads(json.dumps(event))
    client = boto3.client('firehose')

    response = client.put_record(
    DeliveryStreamName=FIREHOSE_DELIVERY_STREAM,
    Record={
        'Data': json.dumps(data)
    }
    )
   
    return {
        "statusCode": 200,
        "body": json.dumps('successfully pushed data')
    }
